Name:       ambience-portugaldador

Summary:    Portugal Dador ambience
Version:    0.0.1
Release:    1
Group:      System/GUI/Other
License:    TBD
Source0:    %{name}-%{version}.tar.bz2
BuildRequires:  qt5-qttools
BuildRequires:  qt5-qttools-linguist
BuildRequires:  qt5-qmake

Requires:   ambienced

%description
This is a Portugal Dador ambience description

%package ts-devel
Summary:   Translation source for Portugal Dador ambience
License:   TBD
Group:     System/GUI/Other

%description ts-devel
Translation source for a Portugal Dador ambience

%prep
%setup -q -n %{name}-%{version}

# >> setup
# << setup

%build
# >> build pre
# << build pre

%qtc_qmake5

%qtc_make %{?_smp_mflags}

# >> build post
# << build post

%install
rm -rf %{buildroot}
# >> install pre
# << install pre
%qmake5_install

# >> install post
# << install post



%files
%defattr(-,root,root,-)
%{_datadir}/ambience/ambience-portugaldador/ambience-portugaldador.ambience
%{_datadir}/ambience/ambience-portugaldador/sounds.index
%{_datadir}/ambience/ambience-portugaldador/images/*
%{_datadir}/ambience/ambience-portugaldador/sounds/*
%{_datadir}/translations/ambience-portugaldador_eng_en.qm

%files -n ambience-portugaldador-ts-devel
%defattr(-,root,root,-)
%{_datadir}/translations/source/ambience-portugaldador.ts
