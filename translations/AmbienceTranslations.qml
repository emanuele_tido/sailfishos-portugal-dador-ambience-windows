import QtQuick 2.0

Item {
    function qsTrIdString() {
        //% "Portugal Dador"
        QT_TRID_NOOP("ambience-portugaldador-name")
        //% "Ringtone"
        QT_TRID_NOOP("portugaldador-ringtone")
        //% "IM tone"
        QT_TRID_NOOP("portugaldador-im")
        //% "Email tone"
        QT_TRID_NOOP("portugaldador-email")
        //% "Message tone"
        QT_TRID_NOOP("portugaldador-message")
        //% "Calendar alarm"
        QT_TRID_NOOP("portugaldador-calendar-alarm")
        //% "Clock Alarm"
        QT_TRID_NOOP("portugaldador-clock-alarm")
    }
}
